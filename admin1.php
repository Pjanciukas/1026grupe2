<?php
session_start();
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Adimin paskyra</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body>
		<div id="page-wrapper">

			<!-- Header -->
				<header id="header">
					<h1 id="logo">Administratorius</h1>
					<nav id="nav">
						<ul>
							<li><a href="index.html">Home</a></li>
							<li>
								<a href="#">Layouts</a>
								<ul>
									<li><a href="left-sidebar.html">Left Sidebar</a></li>
									<li><a href="right-sidebar.html">Right Sidebar</a></li>
									<li><a href="no-sidebar.html">No Sidebar</a></li>
									<li>
										<a href="#">Submenu</a>
										<ul>
											<li><a href="#">Option 1</a></li>
											<li><a href="#">Option 2</a></li>
											<li><a href="#">Option 3</a></li>
											<li><a href="#">Option 4</a></li>
										</ul>
									</li>
								</ul>
							</li>
							<li><a href="elements.html">Elements</a></li>
							<li><a href="#" class="button special">Sign Up</a></li>
						</ul>
					</nav>
				</header>

			<!-- Main -->
				<div id="main" class="wrapper style1">
					<div class="container">
						<header class="major">
							<h2>Admin paskyra</h2>
							
						</header>

						<!-- Content -->
						
						
						
						
						
							<section id="content">
							
							
							<form action="saveNewspaperEntry.php" method="post">
							<textarea cols="50" rows="8" name="entry" maxlength="10000" ></textarea>
							<br>
							<input type="submit" value="Išsaugoti">
		
							</form>
			
							


							
						
						<!-- entryes -->
							<?php


include "config.php";

$sql = "SELECT * FROM newspaper_entry";

$results = $db->query($sql);
if($results->num_rows > 0){
	
	while ($row = $results->fetch_assoc())
	{
		?>
		
		<form action="editNewspaperEntry.php" method="post">
		<p> <?php echo $row['entry_date']; ?> </p>
		<input type="text" name="commentID" value="<?php echo $row['id'];?> ">
		<input type="text" name="entry" value="<?php echo $row['entry'];?>">
		<input type="submit" value="Redaguoti">
		</form>
		
		
	<?php }
	
	}else {
		
		echo "<p>Įrašų nėra</p>";
}
?>
							
								<!-- entryes -->
								
							</section>

					</div>
				</div>

			<!-- Footer -->
				<footer id="footer">
					<ul class="icons">
						<li><a href="#" class="icon alt fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="#" class="icon alt fa-linkedin"><span class="label">LinkedIn</span></a></li>
						<li><a href="#" class="icon alt fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="#" class="icon alt fa-github"><span class="label">GitHub</span></a></li>
						<li><a href="#" class="icon alt fa-envelope"><span class="label">Email</span></a></li>
					</ul>
					<ul class="copyright">
						<li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
					</ul>
				</footer>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>