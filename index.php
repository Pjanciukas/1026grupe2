<?php
header('Content-Type: text/html; charset=utf-8');
session_start();
include "config.php";

?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Naujienų portalas</title>
		<meta http-equiv="content-type" content="text/html; charset=utf8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link href="https://fonts.googleapis.com/css?family=Slabo+27px&amp;subset=latin-ext" rel="stylesheet">
		<link rel="stylesheet" href="assets/css/else.css" />
	</head>
	<body>
		<div id="page-wrapper">

			<!-- Header -->
				<header id="header">
					
					<?php
				if(isset($_SESSION['id'] ) ){
						?>
					
					<h1 id="logo">Laba diena, <?php echo $_SESSION['username']; ?>, esate <?php echo $_SESSION['rights']; ?> </h1>
					<nav id="nav">
						<ul>
							<?php
								if($_SESSION["rights"]== "administratorius"){
									echo '<li><a href="admin.php">Administratorius</a></li>'; 
								} else {
									echo ''; }  ?>
							<li><a href="marketing.php">Reklama</a></li>
							<li><a href="logout.php" class="button special">Atsijungti</a></li>
						</ul>
						</nav>
						
						<?php
				}else{ ?>
						
					<h1 id="logo">Laba diena</h1>
                <nav id="nav">
                            <ul>
                                <li><a href="marketing.php">Reklama</a></li>
								<li><a href="login.php" class="button special">Prisijungti</a></li>
                            </ul>
                </nav>
					<?php } ?>
				</header>

			<!-- Main -->
				<div id="main" class="wrapper">
					<div class="container clear">
						<div class="column-left"><div class="clear"><a href="marketing.php">Čia gali būti jūsų reklama</a></div></div>
						<div class="column-right"><div class="clear"><a href="marketing.php">Čia gali būti jūsų reklama</a></div></div>
						<div class="column-center">
							<div>
								<header class="major">
									<div class="wrapperImg">
									  <div class="headerImg">
										<br>
										<br>
										<h2>Naujienos</h2>
									  </div>
									</div>
								</header>
							
							<!-- Content-->
								<section >
									<div class="clear">
										<h3>Ar Vytautas Šustauskas vis dar be darbo?</h3>
										<div class="pic">
											<a class="crop" href="#" ><img src="https://scontent.xx.fbcdn.net/v/t1.0-9/14264161_549138411963663_8380104972402810113_n.jpg?oh=8856d64d85110ad1e12879f85622ed3f&oe=58BC27B7" alt="" /></a>
										</div>
										<div class="picText">
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean hendrerit tempor leo, ut auctor neque varius vel. Donec pulvinar, elit eget luctus maximus, massa urna porttitor nisi, at posuere mi enim interdum nisl. Cras porta arcu ac elit ornare, non molestie nibh scelerisque. Aliquam ultrices interdum viverra. Duis blandit dictum sem, id rutrum nunc tempus sit amet. Pellentesque eget nulla fringilla, maximus magna nec, luctus eros. Phasellus consectetur enim ut nisi eleifend, et viverra mi porta. Donec pharetra felis commodo pulvinar varius. Vestibulum et erat consequat, ultricies turpis ut, congue neque. Mauris vitae lacinia purus, non sodales mauris. Donec ac mollis tortor. Cras ut volutpat ligula. Aenean iaculis nulla quis dolor consectetur, pulvinar egestas ex iaculis. Vestibulum eu ipsum ac diam blandit mattis et quis nulla. Cras quis pulvinar odio.</p>
										</div>
									</div>
									<div class="clear">
										<h3>Populiarausia facebook partija Lietuvoje pagaliau išleido savo programą.</h3>
										<div class="pic">
											<a class="crop" href="#" ><img src="https://scontent.xx.fbcdn.net/v/t1.0-9/15380580_1560443517306245_4449878293102460834_n.jpg?oh=a45dbe2db94060e807c7b0846d1c6bd8&oe=58EF9F84" alt="" /></a>
										</div>
										<div class="picText">
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean hendrerit tempor leo, ut auctor neque varius vel. Donec pulvinar, elit eget luctus maximus, massa urna porttitor nisi, at posuere mi enim interdum nisl. Cras porta arcu ac elit ornare, non molestie nibh scelerisque. Aliquam ultrices interdum viverra. Duis blandit dictum sem, id rutrum nunc tempus sit amet. Pellentesque eget nulla fringilla, maximus magna nec, luctus eros. Phasellus consectetur enim ut nisi eleifend, et viverra mi porta. Donec pharetra felis commodo pulvinar varius. Vestibulum et erat consequat, ultricies turpis ut, congue neque. Mauris vitae lacinia purus, non sodales mauris. Donec ac mollis tortor. Cras ut volutpat ligula. Aenean iaculis nulla quis dolor consectetur, pulvinar egestas ex iaculis. Vestibulum eu ipsum ac diam blandit mattis et quis nulla. Cras quis pulvinar odio.</p>
										</div>
									</div>
									
									<?php


$sql = "SELECT * FROM newspaper_entry
ORDER BY entry_date DESC";


$results = $db->query($sql);
if($results->num_rows > 0){
	
	while ($row = $results->fetch_assoc())
	{
		?>
		
		<div class="clear:after" align="center" text-align="in" >
		
			<div class="picText">
				<div>
						<p> <?php echo $row['entry_date'].' '; echo $row['username'].' ';?> </p>
						<p> <?php echo $row['entry'];?> </p>
				</div>
				<div class="clear" style="margin: 0 0 100px 0">		
					<form action="editNewspaperEntry.php" method="post">
						
						<input type="hidden" name="entry" id="entry" value="<?php echo $row['entry'];?>">
						<input type="hidden" name="entryID" id="entryID" value="<?php echo $row['id'];?>">
												
						<?php
							if(isset($_SESSION['id'] ) ){
								if($_SESSION["rights"]== "administratorius"){
									echo '<div class="containerInputLeft"> <input type="submit" value="Redaguoti"> </div> '; 
								} else {
									echo ''; }
							}else{ echo ''; } 
						?>
								
					</form>
					<form action="dropEntry.php" method="post">
						<input type="hidden" name="entryID" value=" <?php echo $row['id']; ?> ">						
						<?php
							if(isset($_SESSION['id'] ) ){
								if($_SESSION["rights"]== "administratorius"){
									echo '<div class="containerInputLeft"> <input type="submit" value="Istrinti"> </div> '; 
								} else {
									echo ''; }
							}else{ echo ''; } 
						?>	
					</form>
					
					<form action="comments.php" method="post">
						<input type="hidden" name="entry" id="entry" value="<?php echo $row['entry'];?>">
						<input type="hidden" name="entryId" value="<?php echo $row['id'];?>">
						<div class="containerInputLeft"> 
							<input type="submit" value="Komentarai">
						</div>
					</form>
				</div>
			</div>
		
		</div>
	
	<?php	
	}	
 }
	?>
									
								</section>
							</div>	
							<div class="containerTextWriter clear">
								<?php include "writeNewArticle.php" ?>
							</div>
					</div>
				</div>

			<!-- Footer -->
				<footer id="footer">
					<ul class="icons">
						<li><a href="#" class="icon alt fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="#" class="icon alt fa-linkedin"><span class="label">LinkedIn</span></a></li>
						<li><a href="#" class="icon alt fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="#" class="icon alt fa-github"><span class="label">GitHub</span></a></li>
						<li><a href="#" class="icon alt fa-envelope"><span class="label">Email</span></a></li>
					</ul>
					<ul class="copyright">
						<li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
					</ul>
				</footer>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>
			<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
		<script>tinymce.init({ selector:"textarea" });</script>

	</body>
</html>