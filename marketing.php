<!DOCTYPE HTML>
<html>
	<head>
		<title>Reklama</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link href="https://fonts.googleapis.com/css?family=Slabo+27px&amp;subset=latin-ext" rel="stylesheet">
		<link rel="stylesheet" href="assets/css/else.css" />
	</head>
	<body>
		<div id="page-wrapper">

			<!-- Header -->
				<header id="header">
					<h1 id="logo">Reklama</h1>
					<nav id="nav">
						<ul>
							<li><a href="index.php">Pagrindinis</a></li>
							
						</ul>
					</nav>
				</header>

			<!-- Main -->
				<div id="main" class="wrapper">
					<div class="container1 clear">
						<h1><p class="marketingText">Reklamos paslaugos!</p></h1>
						<p>Sveiki, gali užsisakyti reklamą mūsų puslapyje. Vastupidiselt tavaarusaamale, Lorem Ipsum ei ole lihtsalt suvaline tekst. See sai alguse osakesest klassikalisest ladina kirjandusest, mis on pärit aastast 45 eKr, olles seega rohkem kui kaks tuhat aastat vana. Virginia Hampden-Sydney College'i ladina keele professor Richard McClintock otsis Lorem Ipsumi lõigust ühe ebamääraseima ladinakeelse sõna, 'consecteur', ja vaadates läbi sõna viited klassikalises kirjanduses, leidis Lorem Ipsumi vaieldamatu allika. Lorem Ipsum on tulnud Cicero 45 aastat eKr kirjutatud raamatu "De Finibus Bonorum et Malorum" ("Hüvede ja pahede piiridest") osadest 1.10.32 ja 1.10.33. See raamat on uurimus eetikateooriast, mis oli renessansi ajal väga populaarne. Lorem Ipsumi esimene rida, "Lorem ipsum dolor sit amet...", tuleb reast osas 1.10.32.
						Saadaval on mitmeid erinevaid Lorem Ipsumi variante, kuid enamik neist on läbi teinud muutusi kas sisestatud huumori või suvaliste sõnade tõttu, mis ei näe üldsegi usutavad välja. Kui te kavatsete Lorem Ipsumit kasutada, peate te kindel olema, et kuskil keset teksti midagi piinlikku ei ole. Kõik Lorem Ipsumi generaatorid kipuvad kindlaid lauseridu kordama, seega on see sait siin esimene tõeline Lorem Ipsumi generaator Internetis. See kasutab sõnastikku üle 200 ladina sõnaga ja, koos hulga lausestruktuuride mudelitega, toodab korralikku Lorem Ipsumit. Toodetud Lorem Ipsum on seega alati vaba kordustest, naljadest, ebaharilikest sõnadest jne.</p>
						<ul style="list-style-type:square">
							<li>McClintock</li>
							<li>Ipsumi</li>
							<li>Lorem</li>
						</ul><br>
						<form action="#" method="post"">
							<textarea cols="50" rows="8" name="entry" maxlength="500"></textarea><br>
							<input type="submit" value="išsiųsti pranešimą">
						</form><br>	
					</div>
				</div>

			<!-- Footer -->
				<footer id="footer">
					<ul class="icons">
						<li><a href="#" class="icon alt fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="#" class="icon alt fa-linkedin"><span class="label">LinkedIn</span></a></li>
						<li><a href="#" class="icon alt fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="#" class="icon alt fa-github"><span class="label">GitHub</span></a></li>
						<li><a href="#" class="icon alt fa-envelope"><span class="label">Email</span></a></li>
					</ul>
					<ul class="copyright">
						<li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
					</ul>
				</footer>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>
			<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
			<script>tinymce.init({ selector:"textarea" });</script>

	</body>
</html>