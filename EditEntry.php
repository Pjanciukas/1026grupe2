<?php
session_start();
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Adimin paskyra</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body>
		<div id="page-wrapper">

			<!-- Header -->
				<header id="header">
					<h1 id="logo">Administratorius</h1>
					<nav id="nav">
						<ul>
							<li><a href="index.php">Pagrindinis</a></li>
							<li><a href="logout.php" class="button special">Atsijungti</a></li>
						</ul>
					</nav>
				</header>

			<!-- Main -->
				<div id="main" class="wrapper style1">
					<div class="container">
						<header class="major">
							<h2>Straipsniu redagavimas</h2>					
						</header>
				<?php
					include "config.php";
					$sql = "SELECT * FROM newspaper_entry";
					$results = $db->query($sql);
					if($results->num_rows > 0){
						while ($row = $results->fetch_assoc()) { ?>
		
							<form action="updateNewspaperEntry.php" method="post">
								<p> <?php echo $row['entry_date']; ?> </p>
								<input type="text" name="entryID" value="<?php echo $row['id'];?> ">
								<input type="text" name="entryEdited" value="<?php echo $row['entry'];?>">
								<input type="submit" value="Redaguoti">
							</form> <?php }
	
					}else {
						echo "<p>Ira�u nera</p>"; } ?>
							
								<!-- entryes -->
								
							</section>

					</div>
				</div>

			<!-- Footer -->
				<footer id="footer">
					<ul class="icons">
						<li><a href="#" class="icon alt fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="#" class="icon alt fa-linkedin"><span class="label">LinkedIn</span></a></li>
						<li><a href="#" class="icon alt fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="#" class="icon alt fa-github"><span class="label">GitHub</span></a></li>
						<li><a href="#" class="icon alt fa-envelope"><span class="label">Email</span></a></li>
					</ul>
					<ul class="copyright">
						<li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
					</ul>
				</footer>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>