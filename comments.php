<?php
session_start();
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Naujienų portalas</title>
		<meta http-equiv="content-type" content="text/html; charset=utf8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link href="https://fonts.googleapis.com/css?family=Slabo+27px&amp;subset=latin-ext" rel="stylesheet">
		<link rel="stylesheet" href="assets/css/else.css" />
	</head>
	<body>
		<div id="page-wrapper">

			<!-- Header -->
				<header id="header">
					
					<?php
				if(isset($_SESSION['id'] ) ){
						?>
					
					<h1 id="logo">Laba diena, <?php echo $_SESSION['username']; ?>, esate <?php echo $_SESSION['rights']; ?> </h1>
					<nav id="nav">
						<ul>
							<?php
								if($_SESSION["rights"]== "administratorius"){
									echo '<li><a href="admin.php">Administratorius</a></li>'; 
								} else {
									echo ''; }  ?>
							<li><a href="index.php">Pagrindinis</a></li>		
							<li><a href="marketing.php">Reklama</a></li>
							<li><a href="logout.php" class="button special">Atsijungti</a></li>
						</ul>
						</nav>
						
						<?php
				}else{ ?>
						
					<h1 id="logo">Laba diena</h1>
                <nav id="nav">
                    <ul>
                        
						<li><a href="index.php">Pagrindinis</a></li>
						<li><a href="marketing.php">Reklama</a></li>
						<li><a href="login.php" class="button special">Prisijungti</a></li>
                        <li><a href="signup.php" class="button special">Registruotis</a></li>
                    </ul>
                </nav>
					<?php } ?>
				</header>

			<!-- Main -->
				<div id="main" class="wrapper">
					<div class="container2 clear">
							<div>
								<header class="major">
									<h2>Komentarai</h2>
									
								</header>
									<div align="center">
									<p> <?php $entry=$_POST['entry']; $entryId=$_POST['entryId']; echo $entry; ?> </p></div>
							<!-- Content-->
								<section >
									<div class="clear">
										<div class="containerTextWriter clear">
											<?php include "writeNewComments.php" ?>
										</div>
									</div>
									<?php
										include "config.php";
											$entryId=$_POST['entryId'];
											$sql = "SELECT * FROM newspaper_comments WHERE entry_id='".$entryId."'";	
											$results = $db->query($sql);
											if($results->num_rows > 0){
												while ($row = $results->fetch_assoc()) { 
												
												$username = $row["username"];
												$id = $row["id"];
												$comment = $row["comment"];
												$showComment = $row["showComment"];
												$date = $row["date"];
												?>
												
												<div style="width:40%" class="clear">
													<div>
												
												<?php
												
												if($showComment == "yes"){
														echo '<p>"'.$date.' " Slapyvardis: "' . $username . '". Komentaras: '. $comment .' </p>'; 
														}else{
															echo "";
														}
														 
													?>
													
													</div>
													<form action="dropComment.php" method="post">
													<input name="commentID" type="hidden" value="<?php echo $id; ?> " >
													<?php
														if(isset($_SESSION['id'] ) ){
												
														if($_SESSION["rights"]== "administratorius" && $showComment == "yes"){
														echo '
														<input type="submit" value="istrinti">'; } } ?>
													</form>
													<form action="upadateNewspaperComments.php" method="post">
													<input type="hidden" name="commentID" value="<?php echo $id;?>" >
													<input type="hidden" name="commentID" value="<?php echo $id;?>" >
													
													<?php
													if(isset($_SESSION['id'] ) ){
														if($_SESSION["rights"]== "administratorius"&& $showComment == "yes"){
														echo '<input type="text" name="comment" value="' . $comment.'" ><input type="submit" value="redaguoti">'; } } ?>
													</form>
												</div>
												
												<?php }
											}else {
												echo "<p>Įrašų nėra</p>"; } ?>									
								</section>
							</div>	
					</div>							
				</div>
				
			<!-- Footer -->
				<footer id="footer">
					<ul class="icons">
						<li><a href="#" class="icon alt fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="#" class="icon alt fa-linkedin"><span class="label">LinkedIn</span></a></li>
						<li><a href="#" class="icon alt fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="#" class="icon alt fa-github"><span class="label">GitHub</span></a></li>
						<li><a href="#" class="icon alt fa-envelope"><span class="label">Email</span></a></li>
					</ul>
					<ul class="copyright">
						<li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
					</ul>
				</footer>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>
			<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
		<script>tinymce.init({ selector:"textarea" });</script>

	</body>
</html>