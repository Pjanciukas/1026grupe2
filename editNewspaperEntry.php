<?php
session_start();

?>

<html>
    <head>
        <title>Naujienų portalas</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="assets/css/main.css" />
		<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
		<script>tinymce.init({ selector:'textarea' });</script>
    </head>
    <body class="landing">   
        <div id="page-wrapper">

            <!-- Header -->
            <header id="header">
                <h1 id="logo"> </h1>
                <nav id="nav">
                    <ul>
						<li><a href="index.php">Pagrindinis</a></li>
                        <li><a href="logout.php" class="button special">Atsijungti</a></li>
                    </ul>
                </nav>
            </header>

            <!-- Four -->
            <section id="four" class="wrapper style1 special fade-up">
                <div class="container">
                    <header class="major">
                        <h2>Redagavimas</h2>
						
						
                    </header>
					
					
					
					<?php

include "config.php";


$results = $_POST['entry'];
$entryID = $_POST['entryID'];
		
?>		
		<div>
		<p> <?php echo $results; ?> </p>
		</div>
		
		<form action="updateNewspaperEntry.php" method="post">
			<input type="hidden" name="entryID" value="<?php echo $entryID;?>">
			<textarea cols="30" rows="10" name="entryEdited" maxlength="10000" value="<?php echo $results; ?>"></textarea>
			<br>
			<input type="submit" value="Išsaugoti">
		</form>
							

					
					<div>
					
                </div>
            </section>
            <script>
			
<!-- Footer -->
                < footer id = "footer" >
                        < ul class = "icons" >
                        < li > < a href = "#" class = "icon alt fa-twitter" > < span class = "label" > Twitter < /span></a > < /li>
                                    < li > < a href = "#" class = "icon alt fa-facebook" > < span class = "label" > Facebook < /span></a > < /li>
                                                < li > < a href = "#" class = "icon alt fa-linkedin" > < span class = "label" > LinkedIn < /span></a > < /li>
                                                            < li > < a href = "#" class = "icon alt fa-instagram" > < span class = "label" > Instagram < /span></a > < /li>
                        < li > < a href = "#" class = "icon alt fa-github" > < s pan class = " label" > GitHub  < /span></a > < /li>
                                                                                    < li > < a href = "#" class = "icon alt fa-envelope" > < span class = "label" > Email < /span></a > < /li>
                                                                                                < /ul>
                                                                                                < ul class = "copyright" >
                                                                                                < li > & copy; Untitled.All rights reserved. < /li><li>Design: <a href="http:/ / html5up.net">HTML5 UP</a></li>
                                                                                                < /ul>
                                                                                                < /footer>

                                                                                                < /div>

<!-- Scripts -->
        <script src="assets/js/jquery.min.js"></script>
            <script src="assets/js/jquery.scrolly.min.js"></script>
            <script src="assets/js/jquery.dropotron.min.js"></script>
            <script src="assets/js/jquery.scrollex.min.js"></script>
            <script src="assets/js/skel.min.js"></script>
            <script src="assets/js/util.js"></script>
            <script src="assets/js/main.js"></script>

    </body>
</html>