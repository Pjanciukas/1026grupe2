<?php
session_start();
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Adimin paskyra</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body>
		<div id="page-wrapper">

			<!-- Header -->
				<header id="header">
					<h1 id="logo">Administratorius</h1>
					<nav id="nav">
						<ul>
							<li><a href="index.php">Pagrindinis</a></li>
							<li><a href="admin.php">Administratorius</a></li>
							<li><a href="logout.php" class="button special">Atsijungti</a></li>
						</ul>
					</nav>
				</header>

			<!-- Main -->
				<div id="main" class="wrapper style1">
					<div class="container">
						<header class="major">
							<h2>Teisiu keitimas</h2>					
						</header>
						<div class="clear">
				<?php
					include "config.php";
					$sql = "SELECT * FROM newspaper";
					$results = $db->query($sql);
					if($results->num_rows > 0){
						while ($row = $results->fetch_assoc()) { ?>
		
							<p> <?php echo "Vartotojo ID: ". $row['id']." ."; ?> <?php echo "Vardas: ". $row['username']." ."; ?> <?php echo "Teisės: ". $row['rights']."."; ?> </p>			
							<form method="Post" action="changeSQL.php">
							<input type="hidden" name="ID" value=<?php echo $row['id'];?>>
							<input type="radio" name="rights" value="administratorius">administratorius<br>
							<input type="radio" name="rights" value="rasytojas">rasytojas<br>
							<input type="radio" name="rights" value="skaitytojas">skaitytojas<br><br>
							<input type="Submit" value="Pakeisti teises">
							</form>	
								
								<?php }	
					}else {
						echo "<p>Irasu nera</p>"; } ?>
						</div>
					
					</div>
				</div>

			<!-- Footer -->
				<footer id="footer">
					<ul class="icons">
						<li><a href="#" class="icon alt fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="#" class="icon alt fa-linkedin"><span class="label">LinkedIn</span></a></li>
						<li><a href="#" class="icon alt fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="#" class="icon alt fa-github"><span class="label">GitHub</span></a></li>
						<li><a href="#" class="icon alt fa-envelope"><span class="label">Email</span></a></li>
					</ul>
					<ul class="copyright">
						<li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
					</ul>
				</footer>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>