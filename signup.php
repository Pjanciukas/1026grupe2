
<html>
    <head>
        <title>Registracija</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="assets/css/main.css" />
    </head>
    <body class="landing">   
        <div id="page-wrapper">

            <!-- Header -->
            <header id="header">
                <h1 id="logo"><a href="index.html">Landed</a></h1>
                <nav id="nav">
                    <ul>
                        <li><a href="index.php" class="button special">Prisijūngti</a></li>
                    </ul>
                </nav>
            </header>

            <!-- Four -->
            <section id="four" class="wrapper style1 special fade-up">
                <div class="container">
                    <header class="major">
                        <h2>Prisijungimas</h2>
                        
                       
									
			
		<form action="db.php" method="get">
			<div class="row uniform 50%">					
				 <div class="6u 12u$(xsmall)">
					<input name="username" type="text" placeholder="Vartotojo vardas">
				</div>
				<div class="6u$ 12u$(xsmall)">
					<input name="password" type="password" placeholder="Slaptažodis">
				</div>
				<div class="6u 12u$(xsmall)">
					<input name="name" type="text" placeholder="Vardas">
				</div>
				<div class="6u$ 12u$(xsmall)">
					<input name="lastName" type="text" placeholder="Pavardė">
				</div>
				<div class="6u 12u$(xsmall)">
					<input name="city" type="text" placeholder="Miestas">
				</div>
				<div class="6u$ 12u$(xsmall)">
					<input name="country" type="text" placeholder="Valstybė">
				</div>
								
				<div class="12u$">
                     <ul class="actions">
                         <li><input type="submit" value="Išsiusti" class="special" /></li>
                     </ul>
                </div>
								
								
			
		</form>
			</div>
							
                    </header>
                </div>
            </section>
            <script>
<!-- Footer -->
                < footer id = "footer" >
                        < ul class = "icons" >
                        < li > < a href = "#" class = "icon alt fa-twitter" > < span class = "label" > Twitter < /span></a > < /li>
                                    < li > < a href = "#" class = "icon alt fa-facebook" > < span class = "label" > Facebook < /span></a > < /li>
                                                < li > < a href = "#" class = "icon alt fa-linkedin" > < span class = "label" > LinkedIn < /span></a > < /li>
                                                            < li > < a href = "#" class = "icon alt fa-instagram" > < span class = "label" > Instagram < /span></a > < /li>
                        < li > < a href = "#" class = "icon alt fa-github" > < s pan class = " label" > GitHub  < /span></a > < /li>
                                                                                    < li > < a href = "#" class = "icon alt fa-envelope" > < span class = "label" > Email < /span></a > < /li>
                                                                                                < /ul>
                                                                                                < ul class = "copyright" >
                                                                                                < li > & copy; Untitled.All rights reserved. < /li><li>Design: <a href="http:/ / html5up.net">HTML5 UP</a></li>
                                                                                                < /ul>
                                                                                                < /footer>

                                                                                                < /div>

<!-- Scripts -->
        <script src="assets/js/jquery.min.js"></script>
            <script src="assets/js/jquery.scrolly.min.js"></script>
            <script src="assets/js/jquery.dropotron.min.js"></script>
            <script src="assets/js/jquery.scrollex.min.js"></script>
            <script src="assets/js/skel.min.js"></script>
            <script src="assets/js/util.js"></script>
            <script src="assets/js/main.js"></script>

    </body>
</html>